const fs   = require('fs');
const zlib = require('zlib');

function start() {
  const startDate = process.argv[ 2 ];
  const endDate   = process.argv[ 3 ];

  if (new Date(startDate).toString() === 'Invalid Date' || new Date(endDate).toString() === 'Invalid Date') {
    process.stdout.write(`При вызове скрипта передайте начальную и конечную даты в формате yyyy-MM-ddTHH:mm:ssZ, например:
node script.js "2017-11-14T00:00:00.000Z" "2017-11-15T00:00:00.000Z"`);
    process.exit(1);
  }

  fs.readdir('./', (err, files) => {
    if (err) {
      process.exit(1);
    }

    const filePromises = [];

    for (let i = 0; i < files.length; ++i) {
      if (files[ i ].split('.').length === 1 ||
        files[ i ].split('.')[ files[ i ].split('.').length - 1 ] !== 'gz') {
        continue;
      }
      filePromises.push(processFile(files[ i ], startDate, endDate));
    }

    return Promise.all(filePromises)
      .then(results => {
        results = results.reduce((first, second) => {
          return first.concat(second);
        });

        const report = results.sort().reduce((campaigns, campaignId) => {
          campaignId in campaigns ? campaigns[ campaignId ]++ : campaigns[ campaignId ] = 1;
          return campaigns;
        }, {});

        Object.keys(report).map(campaignId => {
          process.stdout.write(`${campaignId}: ${report[ campaignId ]} \n`);
        });
      });
  });
}

function processFile(path, startDate, endDate) {
  return new Promise((resolve) => {
    const fileStream = fs.createReadStream(path);
    const gzip       = zlib.createUnzip();
    const data       = fileStream.pipe(gzip);
    const fileReport = [];

    data.on('data', (strings) => {
      const objects = strings.toString().split(/\r?\n/);

      for (let i = 0; i < objects.length; ++i) {
        try {
          JSON.parse(objects[ i ]);
        }
        catch (err) {
          process.stderr.write(`Ошибочная строка в файле ${path.split('/')[ path.split('/').length - 1 ]}: "${objects[ i ]}" \n`);
          continue;
        }
        objects[ i ] = JSON.parse(objects[ i ]);
        if (objects[ i ].time >= startDate && objects[ i ].time <= endDate && objects[ i ].event_type === 'show') {
          fileReport.push(objects[ i ].campaign_id);
        }
      }
    });

    data.on('end', () => {
      resolve(fileReport);
    });
  });
}

start();
